package com.ensicaen.criteriahandler.domain

import com.ensicaen.criteriahandler.api.CategoryApi
import com.ensicaen.criteriahandler.spi.CategorySpi

class CategoryApiImpl(private val categorySpi: CategorySpi) : CategoryApi {

    override fun getAll() = categorySpi.getAll()

}
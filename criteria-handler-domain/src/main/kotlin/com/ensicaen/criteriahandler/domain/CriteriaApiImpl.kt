package com.ensicaen.criteriahandler.domain

import com.ensicaen.criteriahandler.api.CriteriaApi
import com.ensicaen.criteriahandler.models.Criteria
import com.ensicaen.criteriahandler.spi.CriteriaSpi

class CriteriaApiImpl(private val criteriaSpi: CriteriaSpi) : CriteriaApi {

    override fun create(criteria: Criteria) {
        criteriaSpi.create(criteria)
    }

    override fun getAll(): List<Criteria> = criteriaSpi.getAll()

    override fun getByCategoryId(categoryId: Int) = criteriaSpi.getByCategoryId(categoryId)

}

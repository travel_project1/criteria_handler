package com.ensicaen.criteriahandler.api

import com.ensicaen.criteriahandler.models.Criteria

interface CriteriaApi {
    fun create(criteria: Criteria)
    fun getAll(): List<Criteria>
    fun getByCategoryId(categoryId: Int): List<Criteria>
}
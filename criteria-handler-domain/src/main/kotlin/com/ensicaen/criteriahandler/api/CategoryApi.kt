package com.ensicaen.criteriahandler.api

import com.ensicaen.criteriahandler.models.Category

interface CategoryApi {
    fun getAll(): List<Category>
}
package com.ensicaen.criteriahandler.spi

import com.ensicaen.criteriahandler.models.Criteria

interface CriteriaSpi {
    fun create(criteria: Criteria)
    fun getAll(): List<Criteria>
    fun getByCategoryId(categoryId: Int): List<Criteria>
}
package com.ensicaen.criteriahandler.spi

import com.ensicaen.criteriahandler.models.Category

interface CategorySpi {
    fun getAll(): List<Category>
}
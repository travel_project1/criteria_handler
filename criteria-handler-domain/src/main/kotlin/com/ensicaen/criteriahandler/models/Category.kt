package com.ensicaen.criteriahandler.models

data class Category(
    val id: Long?,
    val name: String,
    val isMandatory: Boolean,
    val isUnique: Boolean,
)
package com.ensicaen.criteriahandler.models

data class Criteria(
    val id: Int?,
    val name: String,
    val category: Category,
    val hasValue: Boolean,
)
package com.ensicaen.criteriahandler.category

import com.ensicaen.criteriahandler.models.Category
import com.ensicaen.criteriahandler.spi.CategorySpi
import org.springframework.stereotype.Service

@Service
class CategorySpiImpl(private val categoryRepository: CategoryRepository) : CategorySpi {

    override fun getAll(): List<Category> = categoryRepository.findAll().map(CategoryEntity::toDomain)

}
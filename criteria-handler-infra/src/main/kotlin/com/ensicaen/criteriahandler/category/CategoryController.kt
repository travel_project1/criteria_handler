package com.ensicaen.criteriahandler.category

import com.ensicaen.criteriahandler.api.CategoryApi
import com.ensicaen.criteriahandler.api.CriteriaApi
import com.ensicaen.criteriahandler.authentication.getCurrentUser
import com.ensicaen.criteriahandler.criteria.CriteriaController
import com.ensicaen.criteriahandler.extensions.toDto
import com.ensicaen.criteriahandler.models.Category
import com.ensicaen.criteriahandler.models.Criteria
import com.ensicaen.openapi.api.CategoriesApi
import com.ensicaen.openapi.criteriahandler.api.CategoryDto
import com.ensicaen.openapi.criteriahandler.api.CriteriaDto
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


private val LOGGER: Logger = LoggerFactory.getLogger(CriteriaController::class.java)

@RestController
@RequestMapping("/api/v1")
class CategoryController(
    private val categoryApi: CategoryApi,
    private val criteriaApi: CriteriaApi,
) : CategoriesApi {

    override fun getCategories(): ResponseEntity<List<CategoryDto>> {
        val currentUser = getCurrentUser()
        LOGGER.info("Received request from ${currentUser.name} to get category")
        return ok(categoryApi.getAll().map(Category::toDto))
    }

    override fun getCriteriasByCategory(categoryId: Int): ResponseEntity<List<CriteriaDto>> {
        val currentUser = getCurrentUser()
        LOGGER.info("Received request from ${currentUser.name} to get criteria")
        return ok(criteriaApi.getByCategoryId(categoryId).map(Criteria::toDto))
    }

}

package com.ensicaen.criteriahandler.category

import com.ensicaen.criteriahandler.models.Category
import com.sun.istack.NotNull
import javax.persistence.*
import javax.persistence.GenerationType.AUTO

@Entity
@Table(name = "Category")
class CategoryEntity(
    @Column @NotNull val name: String,
    @Column @NotNull val isMandatory: Boolean,
    @Column @NotNull val isUnique: Boolean,
) {
    @Id
    @GeneratedValue(strategy = AUTO)
    var id: Long? = null

    fun toDomain() = Category(id, name, isMandatory, isUnique)
}
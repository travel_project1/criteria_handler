package com.ensicaen.criteriahandler.criteria

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CriteriaRepository : JpaRepository<CriteriaEntity, Long> {
    fun findByCategoryId(categoryId: Long): List<CriteriaEntity>
}
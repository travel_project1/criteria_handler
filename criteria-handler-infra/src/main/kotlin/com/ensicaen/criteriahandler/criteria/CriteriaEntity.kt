package com.ensicaen.criteriahandler.criteria

import com.ensicaen.criteriahandler.category.CategoryEntity
import com.ensicaen.criteriahandler.models.Criteria
import com.sun.istack.NotNull
import javax.persistence.*
import javax.persistence.GenerationType.AUTO

@Entity
@Table(name = "Criteria")
class CriteriaEntity(
    @Column @NotNull val name: String,
    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    val category: CategoryEntity,
    @Column @NotNull val hasValue: Boolean,
) {
    @Id
    @GeneratedValue(strategy = AUTO)
    var id: Long? = null

    fun toDomain() = Criteria(id?.toInt(), name, category.toDomain(), hasValue)
}
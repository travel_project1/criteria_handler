package com.ensicaen.criteriahandler.criteria

import com.ensicaen.criteriahandler.api.CriteriaApi
import com.ensicaen.criteriahandler.authentication.getCurrentUser
import com.ensicaen.criteriahandler.extensions.toDomain
import com.ensicaen.criteriahandler.extensions.toDto
import com.ensicaen.criteriahandler.models.Criteria
import com.ensicaen.openapi.api.CriteriasApi
import com.ensicaen.openapi.criteriahandler.api.CriteriaDto
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


private val LOGGER: Logger = LoggerFactory.getLogger(CriteriaController::class.java)

@RestController
@RequestMapping("/api/v1")
class CriteriaController(private val criteriaApi: CriteriaApi) : CriteriasApi {

    override fun createCriteria(criteriaDto: CriteriaDto): ResponseEntity<Unit> {
        val currentUser = getCurrentUser()
        LOGGER.info("Received request from ${currentUser.name} to add criteria")
        criteriaApi.create(criteriaDto.toDomain())
        return ok().build()
    }

    override fun getCriterias(): ResponseEntity<List<CriteriaDto>> {
        val currentUser = getCurrentUser()
        LOGGER.info("Received request from ${currentUser.name} to get criteria")
        return ok(criteriaApi.getAll().map(Criteria::toDto))
    }

}

package com.ensicaen.criteriahandler.criteria

import com.ensicaen.criteriahandler.extensions.toEntity
import com.ensicaen.criteriahandler.models.Criteria
import com.ensicaen.criteriahandler.spi.CriteriaSpi
import org.springframework.stereotype.Service

@Service
class CriteriaSpiImpl(private val criteriaRepository: CriteriaRepository) : CriteriaSpi {

    override fun create(criteria: Criteria) {
        criteriaRepository.save(criteria.toEntity())
    }

    override fun getAll(): List<Criteria> = criteriaRepository.findAll().map(CriteriaEntity::toDomain)

    override fun getByCategoryId(categoryId: Int): List<Criteria> =
        criteriaRepository.findByCategoryId(categoryId.toLong()).map(CriteriaEntity::toDomain)
}
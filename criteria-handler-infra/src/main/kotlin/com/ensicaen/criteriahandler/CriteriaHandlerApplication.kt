package com.ensicaen.criteriahandler

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CriteriaHandlerApplication

fun main(args: Array<String>) {
    runApplication<CriteriaHandlerApplication>(*args)
}
package com.ensicaen.criteriahandler.extensions

import com.ensicaen.criteriahandler.models.Criteria
import com.ensicaen.openapi.criteriahandler.api.CriteriaDto


fun CriteriaDto.toDomain() = Criteria(id, name, category.toDomain(), hasValue)
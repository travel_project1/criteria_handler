package com.ensicaen.criteriahandler.extensions

import com.ensicaen.criteriahandler.category.CategoryEntity
import com.ensicaen.criteriahandler.models.Category
import com.ensicaen.openapi.criteriahandler.api.CategoryDto

fun Category.toDto() = CategoryDto(name, isMandatory, isUnique, id?.toInt())
fun Category.toEntity() = CategoryEntity(name, isMandatory, isUnique)
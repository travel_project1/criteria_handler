package com.ensicaen.criteriahandler.extensions

import com.ensicaen.criteriahandler.models.Category
import com.ensicaen.openapi.criteriahandler.api.CategoryDto

fun CategoryDto.toDomain() = Category(id?.toLong(), name, isMandatory, isUnique)
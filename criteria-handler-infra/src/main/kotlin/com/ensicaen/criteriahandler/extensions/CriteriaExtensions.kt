package com.ensicaen.criteriahandler.extensions

import com.ensicaen.criteriahandler.criteria.CriteriaEntity
import com.ensicaen.criteriahandler.models.Criteria
import com.ensicaen.openapi.criteriahandler.api.CriteriaDto

fun Criteria.toEntity() = CriteriaEntity(name, category.toEntity(), hasValue)
fun Criteria.toDto() = CriteriaDto(name, category.toDto(), hasValue, id)
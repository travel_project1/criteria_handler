package com.ensicaen.criteriahandler.authentication

import com.ensicaen.criteriahandler.authentication.models.User
import org.springframework.security.core.context.SecurityContextHolder

fun getCurrentUser(): User = SecurityContextHolder.getContext().authentication.principal as User
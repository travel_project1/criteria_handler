package com.ensicaen.criteriahandler.authentication

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod.OPTIONS
import org.springframework.http.HttpStatus.UNAUTHORIZED
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy.STATELESS
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import java.sql.Timestamp
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

private const val UNAUTHORIZED_RESPONSE_CODE = 401

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
class SecurityConfiguration(
    @Autowired var objectMapper: ObjectMapper,
    @Autowired var authenticationProperties: AuthenticationProperties,
    @Autowired var tokenAuthenticationFilter: SecurityFilter,
) : WebSecurityConfigurerAdapter() {

    @Bean
    fun restAuthenticationEntryPoint() =
        AuthenticationEntryPoint { _: HttpServletRequest?, httpServletResponse: HttpServletResponse, _: AuthenticationException? ->
            val errorObject = mutableMapOf(
                "message" to "Unauthorized access of protected resource, invalid credentials",
                "error" to UNAUTHORIZED,
                "code" to UNAUTHORIZED_RESPONSE_CODE,
                "timestamp" to Timestamp(Date().time),
            )
            httpServletResponse.apply {
                contentType = "application/json;charset=UTF-8"
                status = UNAUTHORIZED_RESPONSE_CODE
                writer.write(objectMapper.writeValueAsString(errorObject))
            }
        }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource =
        UrlBasedCorsConfigurationSource().also {
            it.registerCorsConfiguration(
                "/**",
                CorsConfiguration().apply {
                    allowedOrigins = authenticationProperties.allowedOrigins
                    allowedMethods = authenticationProperties.allowedMethods
                    allowedHeaders = authenticationProperties.allowedHeaders
                    allowCredentials = authenticationProperties.allowCredentials
                    exposedHeaders = authenticationProperties.exposedHeaders
                }
            )
        }

    override fun configure(http: HttpSecurity) {
        http.cors().configurationSource(corsConfigurationSource()).and().csrf().disable().formLogin().disable()
            .httpBasic().disable().exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint())
            .and().authorizeRequests()
            .antMatchers(*authenticationProperties.allowedPublicApis.toTypedArray()).permitAll()
            .antMatchers(OPTIONS, "/**").permitAll().anyRequest().authenticated().and()
            .addFilterBefore(tokenAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
            .sessionManagement().sessionCreationPolicy(STATELESS)
    }
}
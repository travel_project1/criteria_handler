package com.ensicaen.criteriahandler.authentication

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class AuthenticationProperties(
    @Value("\${security.allow-credentials}")
    val allowCredentials: Boolean,

    @Value("\${security.allowed-origins}")
    val allowedOrigins: List<String>,

    @Value("\${security.allowed-methods}")
    val allowedMethods: List<String>,

    @Value("\${security.allowed-headers}")
    val allowedHeaders: List<String>,

    @Value("\${security.allowed-public-apis}")
    val allowedPublicApis: List<String>,

    @Value("\${security.exposed-headers}")
    val exposedHeaders: List<String>,

    @Value("\${security.credentials-path}")
    val credentialsPath: String,
)
package com.ensicaen.criteriahandler.configuration

import com.ensicaen.criteriahandler.domain.CategoryApiImpl
import com.ensicaen.criteriahandler.domain.CriteriaApiImpl
import com.ensicaen.criteriahandler.spi.CategorySpi
import com.ensicaen.criteriahandler.spi.CriteriaSpi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DomainConfiguration {

    @Bean
    fun createCriteriaApiBean(criteriaSpi: CriteriaSpi) = CriteriaApiImpl(criteriaSpi)

    @Bean
    fun createCategoryApiBean(categorySpi: CategorySpi) = CategoryApiImpl(categorySpi)

}